package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql" // mysql handler
	"github.com/julienschmidt/httprouter"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*.gohtml"))
}

func startWebsite() {
	fmt.Println("The application has started... Listening on port 9161.")
	router := httprouter.New()
	router.GET("/", homeController)
	router.NotFound = http.HandlerFunc(notFoundController)
	log.Fatal(http.ListenAndServe(":9161", router))
}

func homeController(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("X-Bolyai", "all good things in the bolyai")

	contents, err := ioutil.ReadFile("/var/secrets/bolyaiwtf.json")
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	var config Config
	err = json.Unmarshal(contents, &config)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	db, err := sql.Open(config.Dialect, fmt.Sprintf("%s:%s@/%s", config.User, config.Password, config.Database))


	db.SetMaxIdleConns(0)

	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	pagevars := map[string]interface{}{
		"header":  nil,
		"comment": nil,
	}

	querystring := fmt.Sprintf("SELECT header, comment FROM %s AS r1 JOIN(SELECT CEIL(RAND() * (SELECT MAX(id) FROM %s)) AS id) AS r2 WHERE r1.id >= r2.id ORDER BY r1.id ASC LIMIT 1", config.Table, config.Table)
	var header, comment string
	err = db.QueryRow(querystring).Scan(&header, &comment)

	defer db.Close()

	if (err != nil) {
		log.Fatal(err)
		os.Exit(1)
	}

	pagevars["header"] = template.HTML(header)
	pagevars["comment"] = template.HTML(comment)

	err = tpl.ExecuteTemplate(w, "home.gohtml", pagevars)
	if err != nil {
		log.Fatal(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}
}

func notFoundController(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(404)

	err := tpl.ExecuteTemplate(w, "404.gohtml", nil)
	if err != nil {
		log.Fatal(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}
}
