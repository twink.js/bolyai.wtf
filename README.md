# bolyai.wtf

Because what the actual fuck.

**THIS IS A JOKE REPO FOR A COMPLETELY USELESS PROJECT.**

## Dependencies:

 * Go
 * A web server
 * MySQL

## Installation

Clone the repo:

```
git clone git@gitlab.com:jozsefsallai/bolyai.wtf
cd bolyai.wtf
```

Install the deps:

```
go get github.com/go-sql-driver/mysql
go get github.com/julienschmidt/httprouter
```

Create a new config file in `/var/secrets/bolyaiwtf.json` with this structure:

```json
{
  "Dialect":  "mysql",
  "User":     "mysql user",
  "Password": "mysql pass",
  "Database": "mysql db",
  "Table":    "mysql table"
}
```

Table should have this structure:

 * `id INT AUTO_INCREMENT PRIMARY KEY`
 * `header VARCHAR(255) NOT NULL`
 * `comment VARCHAR(255)`

Build the thing:

```
go build -o bolyaiwtf
```

## Usage

 * `./bolyaiwtf` - starts the website, it will listen on port :9161
 * `./bolyaiwtf add` - add new entry
 * `./bolyaiwtf edit [id]` - edit entry (WIP)
 * `./bolyaiwtf delete [id]` - delete entry

--

All rights reserved.