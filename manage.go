package main

import (
	"bufio"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	_ "github.com/go-sql-driver/mysql" // mysql handler
)

// Config - holds the configuration of the app
type Config struct {
	Dialect  string
	User     string
	Password string
	Database string
	Table    string
}

func main() {
	if len(os.Args) == 1 {
		startWebsite()
		return
	}

	contents, err := ioutil.ReadFile("/var/secrets/bolyaiwtf.json")
	if err != nil {
		log.Fatal(err)
		return
	}

	var config Config
	err = json.Unmarshal(contents, &config)
	if err != nil {
		log.Fatal(err)
		return
	}

	db, err := sql.Open(config.Dialect, fmt.Sprintf("%s:%s@/%s", config.User, config.Password, config.Database))
	db.SetMaxIdleConns(0)

	if err != nil {
		log.Fatal(err)
	}

	action := os.Args[1]

	switch action {
	case "add":
		reader := bufio.NewReader(os.Stdin)

		fmt.Print("\nHeader: ")
		header, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
			return
		}

		fmt.Print("\nComment (optional): ")
		comment, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
			return
		}

		querystring := "INSERT INTO " + config.Table + " (header, comment) VALUES (?, ?)"

		_, err = db.Query(querystring, strings.TrimSuffix(header, "\n"), strings.TrimSuffix(comment, "\n"))
		if err != nil {
			log.Fatal(err)
			return
		}

		fmt.Println("Record added successfully!")
		break

	case "edit":
		if len(os.Args) == 2 {
			fmt.Println("Please provide the ID of the entry.")
			return
		}

		fmt.Println("Under development")
		break

	case "remove":
		fmt.Print("\nProvide the ID of the message you want to remove: ")
		var id int
		_, err := fmt.Scan(&id)
		if err != nil {
			log.Fatal(err)
			return
		}

		fmt.Print("\nAre you sure you want to delete this entry? (yes/no) ")
		var choice string
		_, err = fmt.Scan(&choice)
		if err != nil {
			log.Fatal(err)
			return
		}

		if choice != "yes" {
			return
		}

		querystring := "DELETE FROM " + config.Table + " WHERE id=?"
		_, err = db.Query(querystring, id)
		if err != nil {
			log.Fatal(err)
			return
		}

		fmt.Println("Record deleted successfully.")
		break

	case "list":
		querystring := "SELECT * FROM " + config.Table
		rows, err := db.Query(querystring)

		for rows.Next() {
			var id int
			var header, comment string

			err = rows.Scan(&id, &header, &comment)
			if err != nil {
				log.Fatal(err)
				return
			}

			fmt.Printf("\nID: %d\n - Header: %s\n - Comment: %s\n", id, header, comment)
		}
		break

	default:
		fmt.Println("Unknown operation.")
		break
	}
}
